﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walk : MonoBehaviour {
    public Transform target;
    UnityEngine.AI.NavMeshAgent agent;

    void Start () {
        Reset();
    }

    float lastMoveTime = 0f;

    void Update () {
        if(Time.timeSinceLevelLoad > lastMoveTime+0.1f)
        {
            GotoNextPoint();
            lastMoveTime = Time.timeSinceLevelLoad;
        }
        animator.SetFloat("Speed", agent.velocity.sqrMagnitude/40f);

        //Debug.Log(agent.remainingDistance );
        //  近いとゆっくり
        if (agent.remainingDistance < 2.5f)
        {
            agent.speed = 0;
            animator.SetTrigger("Rest");
        } else if (agent.remainingDistance < 10f)
        {
            agent.speed = 3.5f;
        } else {
            agent.speed = 6f;
        }
    }

    void GotoNextPoint()
    {
        agent.SetDestination(target.position);
    }

    [SerializeField, HideInInspector] Animator animator;

    void Reset()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        animator = GetComponent<Animator>();
    }
}

