﻿using UnityEngine;
using System.Collections;

public class RandomMove : MonoBehaviour {
    void Start () {
        Reset();
	}
	
    float lastMoveTime = 0f;

	void Update () {
        if(Time.timeSinceLevelLoad > lastMoveTime)
        {
            Reset();
        }
	}

    public void Reset()
    {
        this.transform.localPosition = new Vector3(Random.Range(0,5),5,Random.Range(0,-50));
        lastMoveTime = Time.timeSinceLevelLoad+8;
    }

}
