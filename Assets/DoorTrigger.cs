﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    [SerializeField]
    GameObject door;
    // Update is called once per frame
    private void OnTriggerEnter(Collider col)
    {
        door.transform.position += new Vector3(0, 4, 0);
      
    }
}
